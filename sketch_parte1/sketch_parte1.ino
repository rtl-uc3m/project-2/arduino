// --------------------------------------
// Constants
// --------------------------------------
#define CHECK_TIME_PLAYBACK 100
#define SOUND_PIN 11
#define PLAYBACK_LED_PIN 13
#define PLAYBACK_BUTTON_PIN 7
#define HIGHER_PRIORITY_TASK_PERIOD_SEC 0.000250
#define CLOCK_RATE 16000000L

// --------------------------------------
// Global Variables
// --------------------------------------
unsigned long timeOrig;
bool isMuted = false;
bool playbackButtonOldValue = false;

// --------------------------------------
// Function Declarations
// --------------------------------------
void play_bit();
void show_playback_mode();
void change_playback_mode();
void change_and_show_playback_mode();

// --------------------------------------
// Function: setup
// --------------------------------------
void setup ()
{
  // set the pin of the speaker as output
  pinMode(SOUND_PIN, OUTPUT);
  // set the pin of the playback led as output
  pinMode(PLAYBACK_LED_PIN, OUTPUT);
  // set the pin of the playback button as input
  pinMode(PLAYBACK_BUTTON_PIN, INPUT);

  // starts serial communication and sets frequency
  Serial.begin(115200);

  // setup for priority scheduling
  TCCR1B = _BV(WGM12) | _BV(CS12) | _BV(CS10);
  TCCR1A = _BV(COM1A0);
  TIMSK1 = _BV(OCIE1A);
  OCR1A = HIGHER_PRIORITY_TASK_PERIOD_SEC * CLOCK_RATE / 1024.0;

  // get the start time
  timeOrig = millis();
}

// --------------------------------------
// Function: loop
// --------------------------------------
void loop ()
{
  // the time difference
  unsigned long timeDiff;

  // Task: change_and_show_playback_mode
  change_and_show_playback_mode();

  // calculate the time difference
  timeDiff = CHECK_TIME_PLAYBACK - (millis() - timeOrig);

  // add the CHECK_TIME_PLAYBACK to the original time.
  timeOrig = timeOrig + CHECK_TIME_PLAYBACK;
  
  // delay the required time
  delay(timeDiff);
}

// --------------------------------------
// Task: play_bit
// --------------------------------------
void play_bit()
{
  // the mask marks which part of the data is played
  static int mask = 1;
  // the actual song to be played
  static unsigned char data = 0;

  // since char is 8 bits, the maximum is 128. After 128, the mask is reset
  if (mask > 128) { 
      // Get the number of bytes (characters) available for reading from the serial port.
      // This is data that's already arrived and stored in the serial receive buffer (which holds 512 bytes)
      if (Serial.available() > 1) {
        // read one char (8 bits, 128 characters) of the serial port
        data = Serial.read();
      }
    
    // reset the mask
    mask = 1;
  }

  // check if the playback mode is "mute"
  if (isMuted) {
    digitalWrite(SOUND_PIN, LOW);
  } else {
    // write one sample (equals one bit) with the mask
    digitalWrite(SOUND_PIN, (data & mask));
  }

  // double the mask to set the pointer to the next part of the bit sequence.
  // (simply moves the pointer one bit to the left: 00000010 --> 00000100
  mask = (mask << 1);
}

// --------------------------------------
// Task: change_and_show_playback_mode
// --------------------------------------
void change_and_show_playback_mode()
{
  change_playback_mode();
  show_playback_mode();
}

// --------------------------------------
// change_playback_mode
// --------------------------------------
void change_playback_mode()
{
  bool buttonPressed;

  // read the playback button input
  buttonPressed = digitalRead(PLAYBACK_BUTTON_PIN);

  // check if the button is pressed and was not pressed before (that means that the button has actually been pressed not holded)
  if (buttonPressed && !playbackButtonOldValue) {
    // toggle isMuted
    isMuted = !isMuted;
  }

  // the current button value is the old button value in the next iteration
  playbackButtonOldValue = buttonPressed;
}

// --------------------------------------
// show_playback_mode
// --------------------------------------
void show_playback_mode()
{
  // if the system is in "mute" turn the LED on
  if (isMuted) {
    digitalWrite(PLAYBACK_LED_PIN, HIGH);
  }

  // else, turn the LED off
  else {
    digitalWrite(PLAYBACK_LED_PIN, LOW);
  }
}

// --------------------------------------
// interruption_function
// --------------------------------------
ISR(TIMER1_COMPA_vect) {
  // Task: play_bit
  play_bit();
}
